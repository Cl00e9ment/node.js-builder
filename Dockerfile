FROM node:current-alpine

ARG TAG

# make sure NPM and Yarn are installed
RUN which npm yarn \
# update NPM and install PNPM
	&& npm i --location=global npm pnpm \
# install Git
	&& if [ "$TAG" != 'light' ]; then apk add git; \
# install Make, GCC and G++
	if [ "$TAG" != 'git' ]; then apk add make gcc g++; \
# install Python3
	if [ "$TAG" != 'gnu' ]; then apk add python3; \
	fi fi fi
