#!/bin/bash

echo "$DOCKER_HUB_TOCKEN" | docker login -u "$DOCKER_HUB_USERNAME" --password-stdin
# Use docker-container driver to allow useful features (push/multi-platform)
docker buildx create --driver docker-container --use
docker buildx inspect --bootstrap
