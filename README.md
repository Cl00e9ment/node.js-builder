# Node.js Builder

Node.js Alpine image that comes with NPM, Yarn, PNPM, Git, Make, GCC, G++, Python2 and Python3.

[Docker Hub page here](https://hub.docker.com/repository/docker/cl00e9ment/node.js-builder)

## Flavors

|         | :light | :git  | :gnu  | :all / :latest |
| :------ | :----: | :---: | :---: | :------------: |
| NPM     |   ✓    |   ✓   |   ✓   |       ✓        |
| Yarn    |   ✓    |   ✓   |   ✓   |       ✓        |
| PNPM    |   ✓    |   ✓   |   ✓   |       ✓        |
| Git     |        |   ✓   |   ✓   |       ✓        |
| Make    |        |       |   ✓   |       ✓        |
| GCC     |        |       |   ✓   |       ✓        |
| G++     |        |       |   ✓   |       ✓        |
| Python3 |        |       |       |       ✓        |
