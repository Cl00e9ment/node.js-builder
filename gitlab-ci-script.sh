#!/bin/bash

TAG_ARRAY=("$@")
TAG_PREFIX="-t ${DOCKER_HUB_USERNAME}/${CI_PROJECT_NAME}:"
FULL_TAGS="${TAG_ARRAY[@]/#/"$TAG_PREFIX"}"

update-binfmts --enable # Important: Ensures execution of other binary formats is enabled in the kernel
docker buildx build --platform "$CI_BUILD_ARCHS" --progress plain --pull --build-arg TAG="$1" $FULL_TAGS --push .
